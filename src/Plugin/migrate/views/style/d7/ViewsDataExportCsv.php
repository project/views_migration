<?php

namespace Drupal\views_migration\Plugin\migrate\views\style\d7;

/**
 * Migrate Views Style plugin.
 *
 * Picks up necessary changes for Views Data Export that are missed by the
 * views_migration implementation.
 *
 * @MigrateViewsStyle(
 *   id = "views_data_export_csv",
 *   plugin_ids = {
 *    "views_data_export_csv"
 *   },
 *   core = {7},
 * )
 */
class ViewsDataExportCsv extends ViewsDataExport {

  /**
   * {@inheritdoc}
   */
  public function prepareDisplayOptions(array &$display_options) {
    $display_options['style_plugin'] = 'data_export';
    parent::prepareDisplayOptions($display_options);
  }

}
